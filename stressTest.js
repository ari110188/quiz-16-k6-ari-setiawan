import http from 'k6/http';
import { sleep, check } from 'k6';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";
import { textSummary } from "https://jslib.k6.io/k6-summary/0.0.1/index.js";


// Stress Test
export const options = {
  stages: [
    { duration: '2m', target: 50 }, // Simulate 50 users for 2 minute
    { duration: '3m', target: 100 }, // Ramp-up to 100 users for 3 minutes
    { duration: '4m', target: 100 }, // Stay at 100 users for 4 minutes
    { duration: '1m', target: 0 }, // Scale down to 0 users in 1 minute
  ],
};

export default function () {
  let response = http.post('https://run.mocky.io/v3/1b6d9480-44c4-49ae-a095-27e780edf0eb');
  check(response, {
    'is status 200': (r) => r.status === 200,
  });
  sleep(1);
}

export function handleSummary(data) {
  return {
    "ReportStressTest.html": htmlReport(data),
    stdout: textSummary(data, { indent: " ", enableColors: true }),
  };
}
